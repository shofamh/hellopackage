<?php

namespace Afs\Sec;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SecController extends Controller
{
  public function greet(String $sName)
  {
      return 'Hi ' . $sName . '! How are you doing today?';
  }

  public static function calling()
  {
    return 'Hallo Shofa s';
  }
}
